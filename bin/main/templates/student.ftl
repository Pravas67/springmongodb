<!DOCTYPE html>
<html>
    <head>
        <title>Students</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        </head>
    <body>
        <h2>List of Student</h2>
		<#list students as student>
			Name: ${student.name}
			USN: ${student.usn}
			Semester: ${student.sem}
			
			<h3>Subject List with mark</h3>
        	<table>
            	<tr>
                	<th>Name</th>
                	<th>Pass score</th>
                	<th>Score</th>
                	<th>Result</th>
            	</tr>
            	<#list student.subjects as subject>
                	<tr>
                    	<td>${subject.name}</td>
                    	<td>${subject.passScore}</td>
                    	<td>${subject.score}</td>
                    	<td>${subject.result}</td>
                	</tr>
            	</#list>
            	<tr>
            		<th colspan="2">Total score</td>
            		<th>Total score</td>
            	</tr>
        	</table>
		</#list>
    </body>
</html>