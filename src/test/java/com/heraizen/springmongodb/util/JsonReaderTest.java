package com.heraizen.springmongodb.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class JsonReaderTest {
	private Logger logger = LoggerFactory.getLogger(JsonReaderTest.class);

	@Test
	public void testReadTeamByJson() {
		logger.info("testReadTeamByJson method................");
		logger.info("total--------------" + JsonReader.readData("iplinfo.json").size());
		Assertions.assertEquals(8, JsonReader.readData("iplinfo.json").size());
	}
}
