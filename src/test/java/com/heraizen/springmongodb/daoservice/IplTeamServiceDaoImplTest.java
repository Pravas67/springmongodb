package com.heraizen.springmongodb.daoservice;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.heraizen.springmongodb.daoservice.IplTeamServiceDao;
import com.heraizen.springmongodb.domain.Team;
import com.heraizen.springmongodb.exception.DataNotFoundException;
import com.heraizen.springmongodb.exception.TeamLabelNotFoundException;
import com.heraizen.springmongodb.util.JsonReader;

@SpringBootTest
public class IplTeamServiceDaoImplTest {
	private List<Team> teamList = new ArrayList<Team>();
	private Logger logger = LoggerFactory.getLogger(IplStatServiceDaoTest.class);
	@Autowired
	private IplTeamServiceDao iplTeamServiceDaoImpl;

	@BeforeEach
	public void init() {
		logger.info("inside init method");
		iplTeamServiceDaoImpl.removeAllTeams();
		teamList = JsonReader.readData("iplinfo.json");
		iplTeamServiceDaoImpl.insertTeams(teamList);
		logger.info("teamList size........." + teamList.size());
	}

	@DisplayName("Add team list test")
	public void testInsertTeam() {
		Assertions.assertEquals(8, iplTeamServiceDaoImpl.insertTeams(teamList).size());
	}

	@Test
	@DisplayName("find team by label")
	public void testFindTeamByLabel() {
		try {
			Assertions.assertEquals("Mahela Jaywardene", iplTeamServiceDaoImpl.findTeamByLabel("MI").getCoach());
		} catch (TeamLabelNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	@DisplayName("find team by id")
	public void testFindTeamById() {
		try {
			Assertions.assertEquals("Mahela Jaywardene",
					iplTeamServiceDaoImpl.findTeamById(iplTeamServiceDaoImpl.findTeams().get(0).getId()).getCoach());
		} catch (DataNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Test
	@DisplayName("delete team by id")
	public void testDeleteTeamById() {
		try {
			iplTeamServiceDaoImpl.deleteTeam(iplTeamServiceDaoImpl.findTeams().get(0).getId());
		} catch (DataNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Assertions.assertEquals(7, iplTeamServiceDaoImpl.findTeams().size());
	}

	@Test
	@DisplayName("Find all teams")
	public void testFindAllTeams() {
		Assertions.assertEquals(8, iplTeamServiceDaoImpl.findTeams().size());
	}

	@Test
	@DisplayName("Delete player by team id and player name")
	public void testRemovePlayer() {
		int beforeDeletePlayerSize = iplTeamServiceDaoImpl.findTeams().get(0).getPlayers().size();
		logger.info("before delete player size {}", beforeDeletePlayerSize);
		try {
			iplTeamServiceDaoImpl.removePlayer(iplTeamServiceDaoImpl.findTeams().get(0).getId(),
					iplTeamServiceDaoImpl.findTeams().get(0).getPlayers().get(0).getName());
		} catch (DataNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int afterDeletePlayerSize = iplTeamServiceDaoImpl.findTeams().get(0).getPlayers().size();
		logger.info("after delete player size {}", afterDeletePlayerSize);
		Assertions.assertEquals(beforeDeletePlayerSize, afterDeletePlayerSize);
	}
}
