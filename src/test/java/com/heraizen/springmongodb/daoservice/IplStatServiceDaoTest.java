package com.heraizen.springmongodb.daoservice;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.heraizen.springmongodb.daoservice.IplStatServiceDao;
import com.heraizen.springmongodb.daoservice.IplTeamServiceDao;
import com.heraizen.springmongodb.domain.Team;
import com.heraizen.springmongodb.dto.TeamLableDTO;
import com.heraizen.springmongodb.util.JsonReader;
import com.heraizen.springmongodb.util.JsonReaderTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class IplStatServiceDaoTest {
	private List<Team> teamList = new ArrayList<Team>();
	private Logger logger = LoggerFactory.getLogger(IplStatServiceDaoTest.class);
	@Autowired
	private IplStatServiceDao iplStartServiceDaoImpl;
	@Autowired
	private IplTeamServiceDao iplTeamServiceDaoImpl;

	@BeforeEach
	public void init() {
		iplTeamServiceDaoImpl.removeAllTeams();
		teamList = JsonReader.readData("iplinfo.json");
		iplTeamServiceDaoImpl.insertTeams(teamList);
		logger.info("teamList size........." + teamList.size());
	}

	@Test
	@DisplayName("Find team label test")
	public void testFindTeamLabels() {
		TeamLableDTO teamLableDTO = iplStartServiceDaoImpl.findTeamLabels();
		Assertions.assertAll(() -> Assertions.assertEquals(8, teamLableDTO.getLabels().size(), () -> "All labels"));
	}

	@Test
	@DisplayName("Find players by label test")
	public void testFindPlayerByLabel() {
		Assertions.assertAll(
				() -> Assertions.assertEquals(21, iplStartServiceDaoImpl.findPlayerByLabel("RCB").size(),
						() -> "By label RCB"),
				() -> Assertions.assertEquals(24, iplStartServiceDaoImpl.findPlayerByLabel("MI").size(),
						() -> "By label MI"));
	}
	
}
