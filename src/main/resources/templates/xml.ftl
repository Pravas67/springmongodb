<products>
	<#list products as product>
		<product>
			<name>${product.name}</name>
			<price>${product.price}</price>
		</product>
	</#list>
</products>