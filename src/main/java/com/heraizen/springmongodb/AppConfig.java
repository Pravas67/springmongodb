package com.heraizen.springmongodb;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactory;

import com.heraizen.springmongodb.daoservice.IplStatServiceDao;
import com.heraizen.springmongodb.daoservice.IplStatServiceDaoImpl;

@Configuration
public class AppConfig {
	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper;
	}

	@Bean
	@Primary
	public IplStatServiceDao getStatDaoService() {
		IplStatServiceDao iplStatServiceDaoImpl = new IplStatServiceDaoImpl();
		return iplStatServiceDaoImpl;
	}

	@Bean
	public FreeMarkerConfigurationFactory ftlFactory() {
		FreeMarkerConfigurationFactory freeMarkerConfigurationFactory = new FreeMarkerConfigurationFactory();
		freeMarkerConfigurationFactory.setTemplateLoaderPath("classpath:/templates");
		freeMarkerConfigurationFactory.setDefaultEncoding("UTF-8");
		return freeMarkerConfigurationFactory;
	}
}
