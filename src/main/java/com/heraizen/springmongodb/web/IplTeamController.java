package com.heraizen.springmongodb.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.heraizen.springmongodb.domain.Team;
import com.heraizen.springmongodb.exception.DataNotFoundException;
import com.heraizen.springmongodb.exception.TeamAlreadyExistsException;
import com.heraizen.springmongodb.exception.TeamLabelNotFoundException;
import com.heraizen.springmongodb.iplstatservice.IplTeamService;

@RestController
@RequestMapping("/api/v1/team/")
@CrossOrigin(origins = "http://localhost:4200")
public class IplTeamController {
	@Autowired
	private IplTeamService iplTeamService;

	@PostMapping("addteam")
	public ResponseEntity<?> addTeam(@RequestBody Team team) throws TeamAlreadyExistsException {

		team = iplTeamService.addTeam(team);
		return ResponseEntity.ok(team);
	}

	@PostMapping("addteams")
	public List<Team> addTeams(@RequestBody List<Team> team) {
		return iplTeamService.addTeams(team);
	}

	@PutMapping("updateteam")
	public ResponseEntity<?> updateTeam(@RequestBody Team team) throws TeamLabelNotFoundException {
		Team updatedTeam = iplTeamService.updateTeam(team);
		return ResponseEntity.ok(updatedTeam);
	}

	@DeleteMapping("{id}")
	public ResponseEntity<ResponseMessage> deleteTeam(@PathVariable("id") String teamId) throws DataNotFoundException {
		boolean res = iplTeamService.deleteTeam(teamId);
		ResponseMessage  responseMessage=null;		
		if(res) {
			responseMessage = ResponseMessage.builder().message("Deleted successfully").build();
		}else {
			  responseMessage = ResponseMessage.builder().message("Not delted successfully").build();
		}
		return ResponseEntity.ok().body(responseMessage);

	}

	@GetMapping("bylabel/{label}")
	public ResponseEntity<?> findTeamByLabel(@PathVariable("label") String label) throws TeamLabelNotFoundException {
		Team findTeam = iplTeamService.findTeamByLabel(label);
		return ResponseEntity.ok(findTeam);
	}

	@GetMapping("byid/{id}")
	public ResponseEntity<?> findTeamById(@PathVariable("id") String id) throws DataNotFoundException {
		Team findTeam = iplTeamService.findTeamById(id);
		return ResponseEntity.ok(findTeam);
	}

	@GetMapping
	public List<Team> findAllTeams() {
		return iplTeamService.getTeams();
	}

	@DeleteMapping
	public ResponseEntity<ResponseMessage> deleteAllTeam() throws DataNotFoundException {
		boolean res = iplTeamService.removeAllTeams();
		ResponseMessage  responseMessage=null;		
		if(res) {
			responseMessage = ResponseMessage.builder().message("Deleted successfully").build();
		}else {
			  responseMessage = ResponseMessage.builder().message("Not delted successfully").build();
		}
		return ResponseEntity.ok().body(responseMessage);
	}
//	@RequestMapping(value="/upload", method=RequestMethod.POST)
//	public ResponseEntity<Object> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
//		File convertFile = new File("F://Badal Files//"+file.getOriginalFilename());
//		convertFile.createNewFile();
//		FileOutputStream fout = new FileOutputStream(convertFile);
//		fout.write(file.getBytes());
//		fout.close();
//		return new ResponseEntity<>("File is uploaded successfully", HttpStatus.OK);
//	}
}
