package com.heraizen.springmongodb.web;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.heraizen.springmongodb.SpringmongodbApplication;
import com.heraizen.springmongodb.domain.Address;
import com.heraizen.springmongodb.domain.Product;
import com.heraizen.springmongodb.domain.Student;
import com.heraizen.springmongodb.domain.UserProfile;
import com.heraizen.springmongodb.dto.PlayerDTO;
import com.heraizen.springmongodb.dto.RoleAmountDTO;
import com.heraizen.springmongodb.dto.RoleCountDTO;
import com.heraizen.springmongodb.dto.TeamAmountDTO;
import com.heraizen.springmongodb.dto.TeamLableDTO;
import com.heraizen.springmongodb.dto.UserProfileDTO;
import com.heraizen.springmongodb.exception.DataNotFoundException;
import com.heraizen.springmongodb.exception.TeamLabelNotFoundException;
import com.heraizen.springmongodb.iplstatservice.IplStatService;
import com.heraizen.springmongodb.util.JsonReader;

import freemarker.core.ParseException;
import freemarker.template.Configuration;
import freemarker.template.MalformedTemplateNameException;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateNotFoundException;

@RestController
@RequestMapping("/api/v1/stat/")
@CrossOrigin(origins = "http://localhost:4200")
public class IplStatController {
	@Autowired
	SpringmongodbApplication obj;
	@Autowired
	private IplStatService iplStatService;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private Configuration cfg;
	@Autowired
	private JavaMailSender mailSender;

	@GetMapping("labels")
	public ResponseEntity<?> getLabels() throws DataNotFoundException {
		obj.sendEmail();
		TeamLableDTO teamLabels;
		teamLabels = iplStatService.getTeamLabels();
		return ResponseEntity.ok(teamLabels);
	}

	@GetMapping("players/{label}")
	public ResponseEntity<?> getPlayers(@PathVariable("label") String label) throws TeamLabelNotFoundException {
		List<PlayerDTO> playerlist;
		playerlist = iplStatService.getPlayerByLabel(label);
		return ResponseEntity.ok(playerlist);
	}

	@GetMapping("rolecount/{label}")
	public ResponseEntity<?> getRoleCount(@PathVariable("label") String label) throws TeamLabelNotFoundException {
		List<RoleCountDTO> roleCount;
		roleCount = iplStatService.getRoleCountByLabel(label);
		return ResponseEntity.ok(roleCount);
	}

	@GetMapping("playerbylabelandrole/{label}/{role}")
	public ResponseEntity<?> getPlayerByLabelAndRole(@PathVariable("label") String label,
			@PathVariable("role") String role) throws TeamLabelNotFoundException {
		List<PlayerDTO> playerList;
		playerList = iplStatService.getPlayerByLabelAndRole(label, role);
		return ResponseEntity.ok(playerList);
	}

	@GetMapping("amountspentbyteam/{label}")
	public ResponseEntity<?> findAmountSpentByTeam(@PathVariable("label") String label)
			throws DataNotFoundException, TeamLabelNotFoundException {
		TeamAmountDTO teamAmountDTO;
		teamAmountDTO = iplStatService.getAmountSpentByTeam(label);
		return ResponseEntity.ok(teamAmountDTO);
	}

	@GetMapping("amountspentonrolebyteam/{label}")
	public ResponseEntity<?> getAmountSpentOnRoleByLabel(@PathVariable("label") String label)
			throws TeamLabelNotFoundException {
		List<RoleAmountDTO> roleAmount;
		roleAmount = iplStatService.getAmountSpentOnRoleByLabel(label);
		return ResponseEntity.ok(roleAmount);
	}

	@GetMapping("allplayers")
	public ResponseEntity<?> getAllPlayers() throws DataNotFoundException {
		List<PlayerDTO> playerList;
		playerList = iplStatService.getAllPlayers();
		return ResponseEntity.ok(playerList);
	}

	@GetMapping("userprofile/{username}")
	public UserProfileDTO getUserProfile(@PathVariable("username") String userName) {
		Address address = Address.builder().city("Bhadrak").country("India").state("Odisha").build();
		UserProfile userProfile = UserProfile.builder().email("pravasdas958@gmail.com").userName("Pravas")
				.address(address).id("1025").password("123@456pravas").build();
//		TypeMap<UserProfile, UserProfileDTO> typeMap = modelMapper.typeMap(UserProfile.class, UserProfileDTO.class);
//		UserProfileDTO userProfileDTO = typeMap.map(userProfile);
		UserProfileDTO userProfileDTO = modelMapper.map(userProfile, UserProfileDTO.class);
		return userProfileDTO;
	}

	@RequestMapping("/otp")
	public void hello() throws TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException,
			TemplateException {
		Map<String, String> map = new HashMap<String, String>();
		map.put("name", "Pravas");
		map.put("application", "POD");
		map.put("otp", "4562");
		map.put("seconds", "30");
		StringWriter writer = new StringWriter();

		Template template = cfg.getTemplate("otp.ftl");
		template.process(map, writer);
		System.out.println(writer);
	}

	private static List<Product> getProductList() {
		Product p1 = Product.builder().name("Vivo v20").price(25000).build();
		Product p2 = Product.builder().name("Samsung m31").price(19500).build();
		Product p3 = Product.builder().name("one plus node").price(28000).build();
		List<Product> products = Arrays.asList(p1, p2, p3);
		return products;
	}

	@RequestMapping("/product")
	public void getProduct() throws TemplateNotFoundException, MalformedTemplateNameException, ParseException,
			IOException, TemplateException {
		Map<String, List> map = new HashMap<>();

		StringWriter writer = new StringWriter();

		Template template = cfg.getTemplate("order.ftl");
		List<Product> products = getProductList();
		List<String> names = new ArrayList<String>();
		names.add("Pravas");
		map.put("names", names);
		map.put("products", products);
		template.process(map, writer);
		System.out.println(writer);
	}

	@RequestMapping("students")
	public void getStudents() throws TemplateNotFoundException, MalformedTemplateNameException, ParseException,
			IOException, TemplateException {
		List<Student> studentList = JsonReader.getStudentList();
		Map<String, List> map = new HashMap<>();
		Template template = cfg.getTemplate("student.ftl");
		StringWriter writer = new StringWriter();
		map.put("students", studentList);
		template.process(map, writer);
		System.out.println(writer);
	}

	@RequestMapping("student/{usn}")
	public void getStudent(@PathVariable("usn") String usn) throws TemplateNotFoundException,
			MalformedTemplateNameException, ParseException, IOException, TemplateException {
		List<Student> studentList = JsonReader.getStudentList();

		Map<String, List> map = new HashMap<>();
		Template template = cfg.getTemplate("student.ftl");
		StringWriter writer = new StringWriter();
		map.put("students", studentList.stream().filter(s -> s.getUsn().equals(usn)).collect(Collectors.toList()));
		template.process(map, writer);
		System.out.println(writer);
	}

	@RequestMapping("sentmail")
	public String sentMail() {
		MimeMessage message = mailSender.createMimeMessage();

		StringWriter writer = new StringWriter();
		String body = writer.toString();
		Map<String, String> user = new HashMap<String, String>();
		user.put("pravas67", "impravasdas67@gmail.com.com");
		user.put("pravas958", "pravasdas958@gmail.com.com");
		user.entrySet().forEach(u -> {
			MimeMessageHelper helper;
			try {

				helper = new MimeMessageHelper(message, true);
				helper.setFrom("impravasdas1997@gmail.com");
				helper.setTo(u.getValue());
				helper.setSubject("Greeting from Heraizen");
//				helper.setText("<html><head></head><body>Dear </body></html>");
				helper.setText(body, true);
			} catch (MessagingException e2) {
				e2.printStackTrace();
			}
			mailSender.send(message);
		});
		return "Mail Sent successfully";
	}

	@RequestMapping("jsonformatdata")
	public List<Product> getJsonFormatData() {
		List<Product> products = getProductList();
		return products;
	}

	@RequestMapping("xmlformatdata")
	public List<Product> getXMLFormatData() {
		List<Product> products = getProductList();
		return products;
	}

	@RequestMapping("xml")
	public void printProductInXml() {
		Template template;
		try {
			template = cfg.getTemplate("xml.ftl");
			StringWriter writer = new StringWriter();
			Map<String, List> map = new HashMap<>();
			List<Product> products = getProductList();
			map.put("products", products);
			template.process(map, writer);
			System.out.println(writer);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (TemplateException e) {
			e.printStackTrace();
		}
	}

}
