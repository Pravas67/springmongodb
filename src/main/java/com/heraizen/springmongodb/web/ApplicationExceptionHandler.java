package com.heraizen.springmongodb.web;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import com.heraizen.springmongodb.exception.DataNotFoundException;
import com.heraizen.springmongodb.exception.TeamAlreadyExistsException;
import com.heraizen.springmongodb.exception.TeamLabelNotFoundException;

@ControllerAdvice
public class ApplicationExceptionHandler {
	@ExceptionHandler(value = { TeamAlreadyExistsException.class })
	public ResponseEntity<ErrorResponse> handleConflicts(Exception ex, WebRequest request) {
		ErrorResponse errorResponse = ErrorResponse.builder().status(HttpStatus.CONFLICT.value())
				.message(ex.getMessage()).dateTime(LocalDateTime.now()).build();
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.CONFLICT);
	}

	@ExceptionHandler(value = { DataNotFoundException.class })
	public ResponseEntity<ErrorResponse> handleDataNotFound(Exception ex, WebRequest request) {
		ErrorResponse errorResponse = ErrorResponse.builder().status(HttpStatus.CONFLICT.value())
				.message(ex.getMessage()).dateTime(LocalDateTime.now()).build();
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.CONFLICT);
	}

	@ExceptionHandler(value = { TeamLabelNotFoundException.class })
	public ResponseEntity<ErrorResponse> handleLableNotFound(Exception ex, WebRequest request) {
		ErrorResponse errorResponse = ErrorResponse.builder().status(HttpStatus.CONFLICT.value())
				.message(ex.getMessage()).dateTime(LocalDateTime.now()).build();
		return new ResponseEntity<ErrorResponse>(errorResponse, HttpStatus.CONFLICT);
	}
}
