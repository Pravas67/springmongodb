package com.heraizen.springmongodb.iplstatservice;

import java.util.List;

import com.heraizen.springmongodb.domain.Player;
import com.heraizen.springmongodb.domain.Team;
import com.heraizen.springmongodb.exception.DataNotFoundException;
import com.heraizen.springmongodb.exception.TeamAlreadyExistsException;
import com.heraizen.springmongodb.exception.TeamLabelNotFoundException;

public interface IplTeamService {
	public List<Team> addTeams(List<Team> team);

	public Team addTeam(Team team) throws TeamAlreadyExistsException;

	public Team updateTeam(Team team) throws TeamLabelNotFoundException;

	public Team findTeamByLabel(String label) throws TeamLabelNotFoundException;

	public Team findTeamById(String id) throws DataNotFoundException;

	public boolean deleteTeam(String id) throws DataNotFoundException;

	public List<Team> getTeams();

	public boolean removeAllTeams() throws DataNotFoundException;

	public Player addPlayer(String id, Player player) throws DataNotFoundException;

	public Player updatePlayer(String id, Player player) throws DataNotFoundException;

	public boolean removePlayer(String id, String playerName) throws DataNotFoundException;
}
