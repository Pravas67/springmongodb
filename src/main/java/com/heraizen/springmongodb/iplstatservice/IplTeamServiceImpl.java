package com.heraizen.springmongodb.iplstatservice;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import com.heraizen.springmongodb.daoservice.IplTeamServiceDao;
import com.heraizen.springmongodb.domain.Player;
import com.heraizen.springmongodb.domain.Team;
import com.heraizen.springmongodb.exception.DataNotFoundException;
import com.heraizen.springmongodb.exception.TeamAlreadyExistsException;
import com.heraizen.springmongodb.exception.TeamLabelNotFoundException;

@Service
public class IplTeamServiceImpl implements IplTeamService {
	private Logger log = LoggerFactory.getLogger(IplTeamServiceImpl.class);
	@Autowired
	private IplTeamServiceDao iplTeamServiceDaoImpl;

	@Override
	public List<Team> addTeams(List<Team> team) {
		log.info("Total {} teams going to be add", team.size());
		List<Team> teamList = iplTeamServiceDaoImpl.insertTeams(team);
		log.info("Total {} teams is added", teamList.size());
		return teamList;
	}

	@Override
	public Team addTeam(Team team) throws TeamAlreadyExistsException {
		log.info("Team with label {} is going to be add", team.getLabel());
		List<Team> teamList = iplTeamServiceDaoImpl.findTeams();
		teamList.forEach(e -> {
			if (e.getLabel().equalsIgnoreCase(team.getLabel()))
				new TeamAlreadyExistsException("Team with label " + team.getLabel() + " already exits");
		});
		Team addedTeam = iplTeamServiceDaoImpl.insertTeam(team);
		log.info("Team with label {} and id is added successfully", addedTeam.getLabel(), addedTeam.getId());
		return addedTeam;
	}

	@Override
	public Team updateTeam(Team team) throws TeamLabelNotFoundException {
		log.info("Team with label {} is going to be update", team.getLabel());
		Team findTeam = iplTeamServiceDaoImpl.findTeamByLabel(team.getLabel());
		if (findTeam == null) {
			throw new TeamLabelNotFoundException("Team with label " + team.getLabel() + " not found to update");

		}
		team.setId(findTeam.getId());
		try {
			team = iplTeamServiceDaoImpl.insertTeam(team);
		} catch (TeamAlreadyExistsException e) {
			e.printStackTrace();
		}
		log.info("Team with label {} is updated", team.getLabel());
		return team;
	}

	@Override
	public Team findTeamByLabel(String label) throws TeamLabelNotFoundException {
		log.info("Find team by label {} ", label);
		Team team = iplTeamServiceDaoImpl.findTeamByLabel(label);
		log.info("team found with id {} and label {} " + team.getId(), team.getLabel());
		return team;
	}

	@Override
	public Team findTeamById(String id) throws DataNotFoundException {
		log.info("By id {} ", id, " team is going to be searched");
		Team team = null;
		team = iplTeamServiceDaoImpl.findTeamById(id);
		log.info("Team not found with id {} ", id);
		return team;
	}

	@Override
	public boolean deleteTeam(String id) throws DataNotFoundException {
		log.info("By team id {} team is going to be deleted", id);
		iplTeamServiceDaoImpl.deleteTeam(id);
		if (iplTeamServiceDaoImpl.findTeams().size() < 8) {
			log.info("Team deleted successfully");
			return true;
		} else {
			log.info("There are some error while deleting all the teams");
			throw new DataNotFoundException("Team with id " + id + " not found");
		}

	}

	@Override
	public List<Team> getTeams() {
		log.info("Find all teams");
		List<Team> teamList = iplTeamServiceDaoImpl.findTeams();
		log.info("There are {} team found", teamList.size());
		return teamList;
	}

	@Override
	public boolean removeAllTeams() throws DataNotFoundException {
		log.info("All teams are going to be deleted");
		iplTeamServiceDaoImpl.removeAllTeams();
		if (iplTeamServiceDaoImpl.findTeams().size() == 0) {
			log.info("All teams are deleted successfully");
			return true;
		} else {
			log.info("There are some error while deleting all the teams");
			throw new DataNotFoundException("There is some error while deleting the team");
		}
	}

	@Override
	public Player addPlayer(String id, Player player) {
		log.info("With team id {} ", id, " Player with name {} is going to be added", player.getName());

		log.info("Player inserted successfull with id {}", player.getId());
		return player;
	}

	@Override
	public Player updatePlayer(String id, Player player) {
		log.info("With team id {} ", id, " Player with name {} is going to be Updated", player.getName());

		log.info("Player updated successfull with id {}", player.getId());
		return player;
	}

	@Override
	public boolean removePlayer(String id, String playerName) {
		log.info("players with name {} and id {} is going to be deleted", playerName, id);

		log.info("players with name {} and id {} is is deleted successfully", playerName, id);
		return false;
	}

}