package com.heraizen.springmongodb.iplstatservice;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.heraizen.springmongodb.daoservice.IplStatServiceDao;
import com.heraizen.springmongodb.dto.PlayerDTO;
import com.heraizen.springmongodb.dto.RoleAmountDTO;
import com.heraizen.springmongodb.dto.RoleCountDTO;
import com.heraizen.springmongodb.dto.TeamAmountDTO;
import com.heraizen.springmongodb.dto.TeamDTO;
import com.heraizen.springmongodb.dto.TeamLableDTO;
import com.heraizen.springmongodb.exception.DataNotFoundException;
import com.heraizen.springmongodb.exception.TeamLabelNotFoundException;

@Service
public class IplStatServiceImpl implements IplStatService {

	@Autowired
	private IplStatServiceDao iplStartServiceDao;

	private static final Logger log = LoggerFactory.getLogger(IplStatServiceImpl.class);

	@Override
	public TeamLableDTO getTeamLabels() throws DataNotFoundException {
		TeamLableDTO teamLabels = iplStartServiceDao.findTeamLabels();
		if (teamLabels != null) {
			log.info("Total {} team labels found ", teamLabels.getLabels().size());
			return teamLabels;
		}
		log.info("There no teams added yet, and labels return as null");
		throw new DataNotFoundException("There are no labels in data source");
	}

	@Override
	public List<PlayerDTO> getPlayerByLabel(String label) throws TeamLabelNotFoundException {
		Assert.notNull(label, "Label can't be empty or null");

		List<PlayerDTO> players = iplStartServiceDao.findPlayerByLabel(label);
		if (players.isEmpty()) {
			throw new TeamLabelNotFoundException("Team Label not found");
		} else {
			log.info("For given team: {} has total {} player ", label, players.size());
			return players;
		}

	}

	@Override
	public List<RoleCountDTO> getRoleCountByLabel(String label) throws TeamLabelNotFoundException {
		List<RoleCountDTO> roleCounts = iplStartServiceDao.findRoleCountByLabel(label);
		if (roleCounts.isEmpty()) {
			throw new TeamLabelNotFoundException("Team Label Not Found");
		} else {
			log.info("For given team:{} Role count {} ", label, roleCounts);
			return roleCounts;
		}

	}

	@Override
	public List<PlayerDTO> getPlayerByLabelAndRole(String label, String role) throws TeamLabelNotFoundException {
		List<PlayerDTO> players = iplStartServiceDao.findPlayerByLabelAndRole(label, role);
		if (players.isEmpty()) {
			throw new TeamLabelNotFoundException("Team Label and role no found");
		} else {
			log.info("For given team {} and Role {} wise players {} ", label, role, players);
			return players;
		}

	}

	@Override
	public TeamAmountDTO getAmountSpentByTeam(String label) throws DataNotFoundException, TeamLabelNotFoundException {
		TeamAmountDTO spentedAmount = iplStartServiceDao.findAmountSpentByTeam(label);
		if (spentedAmount != null) {
			log.info("{} team spented amount {}", label, spentedAmount);
			return spentedAmount;
		} else {
			throw new DataNotFoundException("Data not found");
		}
	}

	@Override
	public List<RoleAmountDTO> getAmountSpentOnRoleByLabel(String label) throws TeamLabelNotFoundException {

		List<RoleAmountDTO> spentedAmountOnEachRole = iplStartServiceDao.findAmountSpentOnRoleByLabel(label);
		if (!(spentedAmountOnEachRole.isEmpty())) {
			log.info("{} team spented amount on each role {}", label, spentedAmountOnEachRole);
			return spentedAmountOnEachRole;
		} else {
			throw new TeamLabelNotFoundException("Team label not found");
		}

	}

	@Override
	public List<PlayerDTO> getMaxPaidPlayerOnEachRole() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PlayerDTO> getAllPlayers() throws DataNotFoundException {
		List<PlayerDTO> allPlayers = iplStartServiceDao.findAllPlayers();
		if (!(allPlayers.isEmpty())) {
			log.info("All Players Details {}", allPlayers);
			return allPlayers;
		} else {
			throw new DataNotFoundException("Data not found , there");
		}
	}
}
