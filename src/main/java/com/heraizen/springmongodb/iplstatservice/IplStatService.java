package com.heraizen.springmongodb.iplstatservice;

import java.util.List;

import com.heraizen.springmongodb.dto.PlayerDTO;
import com.heraizen.springmongodb.dto.RoleAmountDTO;
import com.heraizen.springmongodb.dto.RoleCountDTO;
import com.heraizen.springmongodb.dto.TeamAmountDTO;
import com.heraizen.springmongodb.dto.TeamDTO;
import com.heraizen.springmongodb.dto.TeamLableDTO;
import com.heraizen.springmongodb.exception.DataNotFoundException;
import com.heraizen.springmongodb.exception.TeamLabelNotFoundException;

public interface IplStatService {
	public TeamLableDTO getTeamLabels() throws DataNotFoundException;

	public List<PlayerDTO> getPlayerByLabel(String label) throws TeamLabelNotFoundException;

	public List<RoleCountDTO> getRoleCountByLabel(String label) throws TeamLabelNotFoundException;

	public List<PlayerDTO> getPlayerByLabelAndRole(String label, String role) throws TeamLabelNotFoundException;

	public TeamAmountDTO getAmountSpentByTeam(String label) throws DataNotFoundException, TeamLabelNotFoundException;

	public List<RoleAmountDTO> getAmountSpentOnRoleByLabel(String label) throws TeamLabelNotFoundException;

	public List<PlayerDTO> getMaxPaidPlayerOnEachRole();

	public List<PlayerDTO> getAllPlayers() throws DataNotFoundException;
}
