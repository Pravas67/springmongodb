package com.heraizen.springmongodb.dto;

import com.heraizen.springmongodb.domain.Address;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserProfileDTO {
	String userName;
	String email;
	Address address;
}
