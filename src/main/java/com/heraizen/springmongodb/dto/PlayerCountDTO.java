package com.heraizen.springmongodb.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PlayerCountDTO {
	String label;
	int count;
}
