package com.heraizen.springmongodb.dto;

import java.util.List;

import com.heraizen.springmongodb.domain.Player;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TeamDTO {
	private String id;
	private String city;
	private String coach;
	private String home;
	private String name;
	private String label;
	private List<Player> players;
}
