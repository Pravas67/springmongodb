package com.heraizen.springmongodb.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PlayerDTO {
	String id;
	String name;
	String role;
	double price;
	String label;
}
