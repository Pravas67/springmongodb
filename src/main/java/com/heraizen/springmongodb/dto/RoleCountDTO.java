package com.heraizen.springmongodb.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RoleCountDTO {
	String role;
	long count;

}
