package com.heraizen.springmongodb.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TeamAmountDTO {
	private String label;
	private double amount;
}
