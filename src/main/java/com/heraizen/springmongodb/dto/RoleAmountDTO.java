package com.heraizen.springmongodb.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RoleAmountDTO {
	String role;
	double amount;

}
