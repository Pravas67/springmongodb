package com.heraizen.springmongodb.daoservice;

import java.util.List;

import com.heraizen.springmongodb.domain.Player;
import com.heraizen.springmongodb.domain.Team;
import com.heraizen.springmongodb.exception.DataNotFoundException;
import com.heraizen.springmongodb.exception.TeamAlreadyExistsException;
import com.heraizen.springmongodb.exception.TeamLabelNotFoundException;

public interface IplTeamServiceDao {
	public List<Team> insertTeams(List<Team> team);

	public Team insertTeam(Team team) throws TeamAlreadyExistsException;

	public Team updateTeam(Team team) throws DataNotFoundException;

	public Team findTeamByLabel(String label) throws TeamLabelNotFoundException;

	public Team findTeamById(String id) throws DataNotFoundException;

	public void deleteTeam(String id) throws DataNotFoundException;

	public List<Team> findTeams();

	public void removeAllTeams();

	public Player insertPlayer(String id, Player player) throws DataNotFoundException;

	public Player updatePlayer(String id, Player player) throws DataNotFoundException;

	public void removePlayer(String id, String playerName) throws DataNotFoundException;
}
