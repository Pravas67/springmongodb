package com.heraizen.springmongodb.daoservice;

import java.util.List;

import com.heraizen.springmongodb.dto.PlayerDTO;
import com.heraizen.springmongodb.dto.RoleAmountDTO;
import com.heraizen.springmongodb.dto.RoleCountDTO;
import com.heraizen.springmongodb.dto.TeamAmountDTO;
import com.heraizen.springmongodb.dto.TeamLableDTO;
import com.heraizen.springmongodb.exception.TeamLabelNotFoundException;

public interface IplStatServiceDao {
	public TeamLableDTO findTeamLabels();

	public List<PlayerDTO> findPlayerByLabel(String label) throws TeamLabelNotFoundException;

	public List<RoleCountDTO> findRoleCountByLabel(String label) throws TeamLabelNotFoundException;

	public List<PlayerDTO> findPlayerByLabelAndRole(String label, String role) throws TeamLabelNotFoundException;

	public TeamAmountDTO findAmountSpentByTeam(String label) throws TeamLabelNotFoundException;

	public List<RoleAmountDTO> findAmountSpentOnRoleByLabel(String label) throws TeamLabelNotFoundException;

	public List<PlayerDTO> findMaxPaidPlayerOnEachRole();

	public List<PlayerDTO> findAllPlayers();
}
