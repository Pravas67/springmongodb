package com.heraizen.springmongodb.daoservice;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.UnwindOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.heraizen.springmongodb.dto.PlayerDTO;
import com.heraizen.springmongodb.dto.RoleAmountDTO;
import com.heraizen.springmongodb.dto.RoleCountDTO;
import com.heraizen.springmongodb.dto.TeamAmountDTO;
import com.heraizen.springmongodb.dto.TeamLableDTO;
import com.heraizen.springmongodb.exception.TeamLabelNotFoundException;

@Repository
@Component
public class IplStatServiceDaoImpl implements IplStatServiceDao {

	private Logger log = LoggerFactory.getLogger(IplStatServiceDaoImpl.class);

	@Autowired
	private MongoOperations monogoOperations;

	@Override
	public TeamLableDTO findTeamLabels() {
		Aggregation query = newAggregation(group("null").addToSet("label").as("labels"), project().andExclude("_id"));
		AggregationResults<TeamLableDTO> result = monogoOperations.aggregate(query, "team", TeamLableDTO.class);
		log.debug("Generated query :{}", query);
		return result.getUniqueMappedResult();

	}

	@Override
	public List<PlayerDTO> findPlayerByLabel(String label) throws TeamLabelNotFoundException {
		MatchOperation match = match(Criteria.where("label").is(label));
		UnwindOperation unwind = unwind("players");
		ProjectionOperation project = project().and("players.name").as("name").and("players.role").as("role")
				.and("players.price").as("price").and("label").as("label").andExclude("_id");

		Aggregation query = newAggregation(match, unwind, project);
		AggregationResults<PlayerDTO> result = monogoOperations.aggregate(query, "team", PlayerDTO.class);

		List<PlayerDTO> playerlist = result.getMappedResults();
		if (playerlist.isEmpty())
			throw new TeamLabelNotFoundException("No team found with label " + label);
		log.info("Total {} players found for the given label: {}", playerlist.size(), label);
		return playerlist;
	}

	@Override
	public List<RoleCountDTO> findRoleCountByLabel(String label) throws TeamLabelNotFoundException {
		MatchOperation match = match(Criteria.where("label").is(label));
		UnwindOperation unwind = unwind("players");
		GroupOperation group = group("players.role").count().as("roleCount");
		ProjectionOperation project = project().and("_id").as("role").and("roleCount").as("count").andExclude("_id");
		Aggregation query = newAggregation(match, unwind, group, project);
		AggregationResults<RoleCountDTO> result = monogoOperations.aggregate(query, "team", RoleCountDTO.class);

		List<RoleCountDTO> roleCounts = result.getMappedResults();
		if (roleCounts.isEmpty())
			throw new TeamLabelNotFoundException("No team found with label " + label);
		log.info("Total {} role count for the given label: ", roleCounts);
		return roleCounts;
	}

	@Override
	public List<PlayerDTO> findPlayerByLabelAndRole(String label, String role) throws TeamLabelNotFoundException {
		MatchOperation matchLabel = match(Criteria.where("label").is(label));
		UnwindOperation unwind = unwind("players");
		MatchOperation matchRole = match(Criteria.where("players.role").is(role));
		ProjectionOperation project = project().and("players.name").as("name").and("players.role").as("role")
				.and("players.price").as("price").and("label").as("label").andExclude("_id");
		Aggregation query = newAggregation(matchLabel, unwind, matchRole, project);

		AggregationResults<PlayerDTO> result = monogoOperations.aggregate(query, "team", PlayerDTO.class);

		List<PlayerDTO> playersRoleWise = result.getMappedResults();
		if (playersRoleWise.isEmpty())
			throw new TeamLabelNotFoundException("No team found with label " + label + " and role " + role);
		log.info("Total {} Player  for the given label and role: ", playersRoleWise.size());
		return playersRoleWise;
	}

	@Override
	public TeamAmountDTO findAmountSpentByTeam(String label) throws TeamLabelNotFoundException {
		MatchOperation matchLabel = match(Criteria.where("label").is(label));
		UnwindOperation unwind = unwind("players");
		GroupOperation group = group("label").sum("players.price").as("totalAmount");
		ProjectionOperation project = project().and("_id").as("label").and("totalAmount").as("amount")
				.andExclude("_id");
		Aggregation query = newAggregation(matchLabel, unwind, group, project);
		log.debug("Generated query" + query);
		AggregationResults<TeamAmountDTO> result = monogoOperations.aggregate(query, "team", TeamAmountDTO.class);

		TeamAmountDTO spentedAmount = result.getUniqueMappedResult();
		if (spentedAmount == null)
			throw new TeamLabelNotFoundException("No team found with label " + label);

		log.info("{} Total amount spented {} : ", spentedAmount.getLabel(), spentedAmount);
		return spentedAmount;
	}

	@Override
	public List<RoleAmountDTO> findAmountSpentOnRoleByLabel(String label) throws TeamLabelNotFoundException {
		MatchOperation matchLabel = match(Criteria.where("label").is(label));
		UnwindOperation unwind = unwind("players");
		GroupOperation group = group("players.role").sum("players.price").as("totalAmount");
		ProjectionOperation project = project().and("_id").as("role").and("totalAmount").as("amount").andExclude("_id");
		Aggregation query = newAggregation(matchLabel, unwind, group, project);
		log.debug("Generated query" + query);
		AggregationResults<RoleAmountDTO> result = monogoOperations.aggregate(query, "team", RoleAmountDTO.class);

		List<RoleAmountDTO> spentedAmountOnEachRole = result.getMappedResults();
		if (spentedAmountOnEachRole.isEmpty())
			throw new TeamLabelNotFoundException("No team found with label " + label);

		log.info("Total amount spented on each role {} : ", spentedAmountOnEachRole);
		return spentedAmountOnEachRole;
	}

	@Override
	public List<PlayerDTO> findMaxPaidPlayerOnEachRole() {

		UnwindOperation unwind = unwind("players");
		GroupOperation group = group("players.role").max("players.price").as("maxPrice");
		ProjectionOperation project = project().and("_id").as("role").and("maxPrice").as("price").andExclude("_id");
		Aggregation query = newAggregation(unwind, group, project);
		log.debug("Generated querys" + query);
		AggregationResults<PlayerDTO> result = monogoOperations.aggregate(query, "team", PlayerDTO.class);

		List<PlayerDTO> maxPaidPlayerForEachRole = result.getMappedResults();

		log.info("Max paid player for each role : ", maxPaidPlayerForEachRole);
		return maxPaidPlayerForEachRole;

	}

	@Override
	public List<PlayerDTO> findAllPlayers() {
		UnwindOperation unwind = unwind("players");
		ProjectionOperation project = project().and("players.name").as("name").and("players.role").as("role")
				.and("players.price").as("price").and("label").as("label").andExclude("_id");
		Aggregation query = newAggregation(unwind, project);
		log.debug("Generated querys" + query);
		AggregationResults<PlayerDTO> result = monogoOperations.aggregate(query, "team", PlayerDTO.class);

		List<PlayerDTO> allPlayers = result.getMappedResults();

		log.info("Total {} players : ", allPlayers.size());
		return allPlayers;
	}

}
