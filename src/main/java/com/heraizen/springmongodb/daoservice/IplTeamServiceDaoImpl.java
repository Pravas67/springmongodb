package com.heraizen.springmongodb.daoservice;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.heraizen.springmongodb.domain.Player;
import com.heraizen.springmongodb.domain.Team;
import com.heraizen.springmongodb.exception.DataNotFoundException;
import com.heraizen.springmongodb.exception.TeamAlreadyExistsException;
import com.heraizen.springmongodb.exception.TeamLabelNotFoundException;
import com.heraizen.springmongodb.repo.IplStartRepo;

@Repository
public class IplTeamServiceDaoImpl implements IplTeamServiceDao {
	private Logger log = LoggerFactory.getLogger(IplStatServiceDaoImpl.class);
	@Autowired
	private MongoOperations mongoOperations;

	@Autowired
	private IplStartRepo iplStartRepo;

	@Override
	public List<Team> insertTeams(List<Team> team) {
		log.info("Total {} teams going to be add", team.size());
		List<Team> teamList = iplStartRepo.saveAll(team);
		log.info("Total {} teams is added", teamList.size());
		return teamList;
	}

	@Override
	public Team insertTeam(Team team) throws TeamAlreadyExistsException {
		log.info("Team with label {} is going to be add", team.getLabel());
		List<Team> teamList = iplStartRepo.findAll();
		teamList.forEach(e -> {
			if (e.getLabel().equalsIgnoreCase(team.getLabel()))
				new TeamAlreadyExistsException("Team with label " + team.getLabel() + " already exits");
		});
		Team addedTeam = iplStartRepo.save(team);
		log.info("Team with label {} and id is added successfully", addedTeam.getLabel(), addedTeam.getId());
		return addedTeam;
	}

	@Override
	public Team updateTeam(Team team) throws DataNotFoundException {
		log.info("Team with label {} is going to be update", team.getLabel());
		Query query = new Query();
		query.addCriteria(Criteria.where("label").is(team.getLabel()));
		Update update = new Update();
		update.set("name", team.getName());
		update.set("city", team.getCity());
		update.set("coach", team.getCoach());
		update.set("label", team.getLabel());
		update.set("home", team.getHome());
		update.set("Players", team.getPlayers());
		team = mongoOperations.findAndModify(query, update, Team.class, "team");
		if (team == null) {
			throw new DataNotFoundException("Team not found to update");
		}
		log.info("Team with label {} is updated , with id {}", team.getLabel(), team.getId());
		return team;
	}

	@Override
	public Team findTeamByLabel(String label) throws TeamLabelNotFoundException {
		log.info("Find team by label {} ", label);
		Team team = iplStartRepo.findByLabel(label);
		if (team == null)
			throw new TeamLabelNotFoundException("No team found with label " + label);

		log.info("team found with id {} and label {} " + team.getId(), team.getLabel());
		return team;
	}

	@Override
	public Team findTeamById(String id) throws DataNotFoundException {
		log.info("By id {} ", id, " team is going to be searched");
		Optional<Team> team = iplStartRepo.findById(id);
		if (team.isPresent()) {
			log.info("Team is found with label {} ", team.get().getLabel());
			return team.get();
		}
		log.info("Team not found with id {} ", id);
		throw new DataNotFoundException("team not found by id " + id);
	}

	@Override
	public void deleteTeam(String id) {
		log.info("By team id {} team is going to be deleted", id);
		iplStartRepo.deleteById(id);
		log.info("remaining team size {}", iplStartRepo.findAll().size());
		log.info("Team deleted successfully");
	}

	@Override
	public List<Team> findTeams() {
		log.info("Find all teams");
		List<Team> teamList = iplStartRepo.findAll();
		log.info("There are {} team found", teamList.size());
		return teamList;
	}

	@Override
	public void removeAllTeams() {
		log.info("All teams are going to be deleted");
		iplStartRepo.deleteAll();
		if (iplStartRepo.findAll().size() == 0) {
			log.info("All teams are deleted successfully");
		} else {
			log.info("There are some error while deleting all the teams");
		}

	}

	@Override
	public Player insertPlayer(String id, Player player) {
		log.info("With team id {} ", id, " Player with name {} is going to be added", player.getName());
	
		log.info("Player inserted successfull with id {}", player.getId());
		return player;
	}

	@Override
	public Player updatePlayer(String id, Player player) {
		log.info("With team id {} ", id, " Player with name {} is going to be Updated", player.getName());

		log.info("Player updated successfull with id {}", player.getId());
		return player;
	}

	@Override
	public void removePlayer(String id, String playerName) {
		log.info("players with name {} and id {} is going to be deleted", playerName, id);
		mongoOperations.findAndRemove(
				new Query(Criteria.where("_id").is(id).elemMatch(Criteria.where("name").is(playerName))), Team.class);
		log.info("players with name {} and id {} is is deleted successfully", playerName, id);

	}

}