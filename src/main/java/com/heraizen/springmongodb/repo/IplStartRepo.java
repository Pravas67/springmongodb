package com.heraizen.springmongodb.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import com.heraizen.springmongodb.domain.Player;
import com.heraizen.springmongodb.domain.Team;

public interface IplStartRepo extends MongoRepository<Team, String> {
	Team findByLabel(String label);
}
