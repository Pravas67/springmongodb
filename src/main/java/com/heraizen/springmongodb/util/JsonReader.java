package com.heraizen.springmongodb.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.heraizen.springmongodb.domain.Student;
import com.heraizen.springmongodb.domain.Team;

@Component
public class JsonReader {
	private JsonReader() {

	}

	public static List<Team> readData(String fileName) {
		return readTeamByJson(fileName);
	}

	private static List<Team> readTeamByJson(String FILE_NAME) {
		List<Team> teamList = new ArrayList<Team>();

		ObjectMapper mapper = new ObjectMapper();

		try {
			teamList = mapper.readValue(JsonReader.class.getResourceAsStream("/iplinfo.json"),
					new TypeReference<List<Team>>() {
					});

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return teamList;
	}

	public static List<Student> getStudentList() {
		List<Student> students = new ArrayList<Student>();

		ObjectMapper mapper = new ObjectMapper();

		try {
			students = mapper.readValue(JsonReader.class.getResourceAsStream("/student.json"),
					new TypeReference<List<Student>>() {
					});

		} catch (IOException e) {
			e.printStackTrace();
		}
		return students;
	}
}
