package com.heraizen.springmongodb.domain;

import lombok.Data;

@Data
public class Player {
	private String id;
	private String name;
	private double price;
	private String role;
}
