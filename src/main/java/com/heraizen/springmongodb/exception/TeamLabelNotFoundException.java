package com.heraizen.springmongodb.exception;

public class TeamLabelNotFoundException extends Exception {
	private static final long serialVersionUID = -2140796233530941955L;

	public TeamLabelNotFoundException(String message) {
		super(message);
	}
}
